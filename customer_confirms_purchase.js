var Web3 = require("web3");

var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

var remotepurchaseContract = web3.eth.contract([{"constant":true,"inputs":[],"name":"sellerDeposit","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"price","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"buyerDeposit","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"confirmReception","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[],"name":"confirmPurchase","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[],"name":"cancelPurchaseContract","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_price","type":"uint256"}],"name":"blockSellerDeposit","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[],"name":"PurchaseContractCreated","type":"event"},{"anonymous":false,"inputs":[],"name":"PurchaseContractCancelled","type":"event"},{"anonymous":false,"inputs":[],"name":"PurchasePaymentConfirmed","type":"event"},{"anonymous":false,"inputs":[],"name":"PurchaseCompleted","type":"event"}]);
var remotepurchase = remotepurchaseContract.at("");


remotepurchase.confirmPurchase({
  from: web3.eth.accounts[1], // <<<<<<< IMPORTANT: The customer should be using a different account! 
  value: 200,
  gas: 65000
}, function(error, result){
  if (!error)
    console.log(result);
  else
    console.log(error);
 });
